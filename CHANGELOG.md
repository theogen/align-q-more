# Changelog

[keepachangelog]: http://keepachangelog.com/en/1.0.0/
[semver]: http://semver.org/spec/v2.0.0.html

All notable changes to this project will be documented in this file.

The changelog format is based on [Keep a Changelog][keepachangelog]. \
This project adheres to [Semantic Versioning][semver].


## [1.1.0] - 2020-05-24

### Added
- Music.
- Win sound.
- Mute music with M.


## [1.0.0] - 2020-05-17

Game release.

<!--
vim: tw=78 ts=2 sw=2 sts=2 et nonu
-->
