-- title:  Align Q More
-- author: Theogen Ratkin
-- desc:   Align MORE planets.
-- script: lua

-- Center coordinates.
cx, cy = 240 / 2, 136 / 2

-- Alignment angle tolerance in radians.
tolerance = .05
-- Revolutions counter.
revol = 0
-- Planet used to count revolutions.
mark = 3
-- Selected planets.
sel = {}
-- Add extra planet.
more = true
-- Whether the planets have been aligned.
win = false
-- Mute music.
mute = false
-- Time passed after winning.
wint = 0

-- Planet definitions.
planets = {
	{ orbit = 20, size = 2, color = 4};
	{ orbit = 37, size = 3, color = 2};
	{ orbit = 52, size = 3, color = 6};
	{ orbit = 64, size = 4, color = 10};
}


function randomize()
	-- Random rotation.
	for i, planet in pairs(planets) do
		planet.angle = math.random() * math.pi * 2
		sel[i] = false
	end
end


randomize()


music(0)


function TIC()
	input()
	update()
	draw()
end


function input()
	if not win then
		-- Toggle selection. Buttons:
		-- 4 = green, 5 = red, 6 = blue, 7 = yellow
		if btnp(7) then sel[1] = not sel[1] end
		if btnp(5) then sel[2] = not sel[2] end
		if btnp(4) then sel[3] = not sel[3] end
		if btnp(6) and more then sel[4] = not sel[4] end

		-- Add extra planet.
		if btnp(0) or btnp(3) then more = true end
		if btnp(1) or btnp(2) then more = false end
	elseif btnp(4) then
		win = false
		wint = 0
		revol = 0
		randomize()
	end

	-- Mute on M.
	if keyp(13) then
		mute = not mute
		music(mute and -1 or 0)
	end
end


function update()
	if win then
		wint = wint + 1
		if wint == 10 then
			sfx(1, 'F#5', -1, 1)
		end
		return
	end

	-- Updating rotations.
	for i, p in pairs(planets) do
		speed = sel[i] and 2 or 1
		p.angle = p.angle + (cy - p.orbit) / 6000 * speed
		if i == mark and p.angle >= math.pi * 2 then
			revol = revol + 1
		end
		p.angle = p.angle % (math.pi * 2)
	end

	-- Planets aligned.
	if aligned() then
		for i, planet in pairs(planets) do
			planet.angle = planets[mark].angle
		end
		win = true
		sfx(1, 'C#5', -1, 1)
	end
end


function draw()
	cls()

	-- Sun.
	circ(cx, cy, 10, 3)

	-- Drawing planets.
	for i, p in pairs(planets) do
		if not more and i == #planets then
			break
		end

		-- Orbit.
		orbitcol = sel[i] and 1 or 15
		circb(cx, cy, p.orbit, orbitcol)

		-- Mark line.
		if i == mark then
			line(cx + p.orbit - 2, cy, cx + p.orbit + 2, cy, orbitcol)
		end

		-- Planet.
		cos = math.cos(p.angle) * p.orbit
		sin = math.sin(p.angle) * p.orbit
		circ(cx + cos, cy + sin, p.size, p.color)
	end

	if win then
		line(cx + cos * 2, cy + sin * 2, cx - cos * 2, cy - sin * 2, 12)
		print("Success!", 2, cy)
	end

	-- Revolutions count.
	local last = more and #planets or #planets - 1
	print(revol, cx + planets[last].orbit + 12, cy)
end


function aligned()
	for i, planet in pairs(planets) do
		if not more and i == #planets then
			break
		end
		if math.abs(planets[1].angle - planet.angle) > tolerance then
			return false
		end
	end
	return true
end

-- <WAVES>
-- 000:00000000000123444444444444321000
-- 001:12230455555544033222333333333211
-- </WAVES>

-- <SFX>
-- 000:000000000000000000000000000000000000000000000000000000000000000000000000100020003000400050006000700080009000a000b000c000400000000000
-- 001:81008100810081008100810081008100810081008100810081008100910091009100a100b100b100b100c100d100d100d100e100e100e100f100f100405000000000
-- 063:000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000304000000000
-- </SFX>

-- <PATTERNS>
-- 000:400006000000600006000000900006000000b00006000000400006000000600006000000b00006000000900006000000400006000000600006000000f00006000000d00006000000f00006000000400008000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
-- </PATTERNS>

-- <TRACKS>
-- 000:100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000293260
-- 001:200000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000300
-- </TRACKS>

-- <PALETTE>
-- 000:1a1c2c5d275db13e53ef7d57ffcd75a7f07038b76425717929366f3b5dc941a6f673eff7f4f4f494b0c2566c86333c57
-- </PALETTE>

