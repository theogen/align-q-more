# Align Q More

[alignq]: https://gitlab.com/thratkin/align-q
[jam]: https://itch.io/jam/tweettweetjam-4
[tic80]: https://tic.computer
[palette]: https://lospec.com/palette-list/sweetie-16
[grafxkid]: https://grafxkid.tumblr.com/palettes

![banner](media/banner.png)

Align **more** planets! Or don't. Just enjoy a nice pixelated simulation of a
star system.

This is an extended version of the game [Align Q][alignq] originally created
for [TweetTweetJam 4][jam] with less than 560 characters of code. This version was made
without the character limitation but I still tried to keep it as minimal as
possible.

## Controls

\<Z\> or \<Button 1\> - Toggle acceleration of green planet.

\<X\> or \<Button 2\> - Toggle acceleration of red planet.

\<A\> or \<Button 3\> - Toggle acceleration of blue planet.

\<S\> or \<Button 4\> - Toggle acceleration of yellow planet.

\<Down\> or \<Left\> - Switch to 3 planets.

\<Up\> or \<Right\> - Switch to 4 planets.

## Credits

Created by Theogen Ratkin.

Made with [TIC-80][tic80].

Palette used: [Sweetie 16][palette] by [GrafxKid][grafxkid].
